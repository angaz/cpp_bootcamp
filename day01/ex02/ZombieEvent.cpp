#include "ZombieEvent.hpp"

std::string ZombieEvent::type = "basic";

ZombieEvent::ZombieEvent() {
}

ZombieEvent::~ZombieEvent() {
}

void ZombieEvent::setZombieType(std::string type) {
    ZombieEvent::type = type;
}

Zombie* ZombieEvent::newZombie(std::string name) {
    Zombie::Zombie* zombie = new Zombie::Zombie(name, ZombieEvent::type);
    zombie->announce();
    return zombie;
}