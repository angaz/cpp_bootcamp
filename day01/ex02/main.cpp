#include "Zombie.hpp"
#include "ZombieEvent.hpp"
#include <cstdlib>

void randomChump(ZombieEvent::ZombieEvent& zombieEvent) {
    srand(time(0));
    int n = 0 + (rand() % (5)); //make random
    std::string names[5] = {"Bob", "Steve", "Garfunkel", "Delilah", "Greenman"};

    zombieEvent.setZombieType("random");
    Zombie::Zombie* zombie = zombieEvent.newZombie(names[n]);
    zombieEvent.setZombieType("basic");
    delete zombie;
}

int main() {
    ZombieEvent::ZombieEvent zombieEvent = ZombieEvent::ZombieEvent(); 
    randomChump(zombieEvent);
    Zombie::Zombie *zombie = zombieEvent.newZombie("Nigel");
    delete zombie;

    return 0;
}

