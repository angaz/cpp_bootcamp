#ifndef ZOMBIE_HORDE_H
# define ZOMBIE_HORDE_H

#include "Zombie.hpp"

class ZombieHorde {

public:

    ZombieHorde(int n);
    ~ZombieHorde();

    void announce();

private:

    Zombie **_horde;
    int _hordeSize;

};
#endif