#include "ZombieHorde.hpp"

ZombieHorde::ZombieHorde(int n) {
    srand(time(0));
    ZombieHorde::_hordeSize = n;
    Zombie *horde[n];
    std::string names[10] = {"Bob", "Steve", "Garfunkel", "Delilah", "Greenman", "Windows", "BoomBoom", "Simon", "TaZerFace", "Anita"};

    for(int i = 0; i < n; i++) {
        horde[i] = new Zombie::Zombie(names[(rand() % 5)], "horde");
    }

    this->_horde = horde;
}

ZombieHorde::~ZombieHorde() {
    for(int i = 0; i < this->_hordeSize; i++) {
        delete this->_horde[i];
    }

    std::cout << "NO MORE HORDE\n";
}

void ZombieHorde::announce() {
    for(int i = 0; i < this->_hordeSize; i++) {
        this->_horde[i]->announce();
    }
}