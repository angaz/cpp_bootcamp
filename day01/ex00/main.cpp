#include "Pony.hpp"

Pony* ponyOnTheHeap(std::string color) {
    return (new Pony::Pony("Heap-Pony", color));
}

Pony ponyOnTheStack(std::string color) {
    return (Pony("Stack-Pony", color));
}

int main() {

    Pony* heapPony = ponyOnTheHeap("gray");
    Pony stackPony = ponyOnTheStack("brown");

    delete heapPony;

    return 0;
}