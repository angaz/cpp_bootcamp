#ifndef PONY_H
#define PONY_H

#include <iostream>
#include <string>

class Pony {

public:

    Pony( std::string name, std::string color);

    ~Pony( void );

private:

    std::string name;
    std::string colour;

};

#endif //PONY_H
