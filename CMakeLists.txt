cmake_minimum_required(VERSION 3.6)
project(cpp_bootcamp)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES
    day01/ex00/main.cpp
    day01/ex00/Pony.cpp
    day01/ex00/Pony.hpp
    day01/ex01/ex01.cpp
    day01/ex02/main.cpp
    day01/ex02/Zombie.cpp
    day01/ex02/Zombie.hpp
    day01/ex02/ZombieEvent.cpp
    day01/ex02/ZombieEvent.hpp
    day01/ex03/main.cpp
    day01/ex03/Zombie.cpp
    day01/ex03/Zombie.hpp
    day01/ex03/ZombieHorde.cpp
    day01/ex03/ZombieHorde.hpp
    day03/ex00/FragTrap.cpp
    day03/ex00/FragTrap.hpp
    day03/ex00/main.cpp
    day03/ex01/main.cpp
    day03/ex01/ScavTrap.cpp
    day03/ex01/ScavTrap.hpp
    day03/ex02/ClapTrap.cpp
    day03/ex02/ClapTrap.hpp
    day03/ex02/FragTrap.cpp
    day03/ex02/FragTrap.hpp
    day03/ex02/main.cpp
    day03/ex02/ScavTrap.cpp
    day03/ex02/ScavTrap.hpp
    day03/ex03/ClapTrap.cpp
    day03/ex03/ClapTrap.hpp
    day03/ex03/FragTrap.cpp
    day03/ex03/FragTrap.hpp
    day03/ex03/main.cpp
    day03/ex03/NinjaTrap.cpp
    day03/ex03/NinjaTrap.hpp
    day03/ex03/ScavTrap.cpp
    day03/ex03/ScavTrap.hpp
    day03/ex04/ClapTrap.cpp
    day03/ex04/ClapTrap.hpp
    day03/ex04/FragTrap.cpp
    day03/ex04/FragTrap.hpp
    day03/ex04/main.cpp
    day03/ex04/NinjaTrap.cpp
    day03/ex04/NinjaTrap.hpp
    day03/ex04/ScavTrap.cpp
    day03/ex04/ScavTrap.hpp
    day03/ex04/SuperTrap.cpp
    day03/ex04/SuperTrap.hpp)

add_executable(cpp_bootcamp ${SOURCE_FILES})