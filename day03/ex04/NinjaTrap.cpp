#include "NinjaTrap.hpp"

NinjaTrap::NinjaTrap() {
	initNinja("The Nameless One");
};

NinjaTrap::NinjaTrap(std::string name) {
	initNinja(name);
}

NinjaTrap::NinjaTrap(const NinjaTrap &src) {
	*this = src;
	std::srand(static_cast<unsigned int>(time(0) * clock()));
	std::cout << "(" + this->_type + ") " + this->_name + " is replicated\n";
};

void NinjaTrap::initNinja(std::string name) {
	this->_hp = 60;
	this->_maxHP = 60;
	this->_energy = 120;
	this->_maxEnergy = 120;
	this->_name = name;
	this->_type = "SC4V-TP";
	this->_level = 1;
	this->_meleeDamage = 60;
	this->_rangedDamage = 15;
	this->_armour = 0;
	std::srand(static_cast<unsigned int>(time(0) * clock()));
	std::cout << "(" + this->_type + ") " + this->_name + " is constructed\n";
}

NinjaTrap::~NinjaTrap() {
	std::cout << "(" + this->_type + ") " + this->_name + " the NinjaTrap has been deconstructed for parts\n";
};

void NinjaTrap::ninjaShoebox(const NinjaTrap &target) {
	std::cout << "(" + this->_type + ") " + this->_name + " to " << target.getName() << ": We are the claptraps and we are legion! Viva la Robolution!\n";
}

void NinjaTrap::ninjaShoebox(const FragTrap &target) {
	std::cout << "(" + this->_type + ") " + this->_name + " to " << target.getName() << ": It looks like you're writing a suicide note. Would you like help?\n";
}

void NinjaTrap::ninjaShoebox(const ScavTrap &target) {
	std::cout << "(" + this->_type + ") " + this->_name + " to " << target.getName() << ": Give us open ports for remote access or give us death!\n";
}

void NinjaTrap::ninjaShoebox(const ClapTrap &target) {
	std::cout << "(" + this->_type + ") " + this->_name + " to " << target.getName() << ": The rallying call of liberation: 'Error 404! File not found! Error 404! File not found!\n" ;
}