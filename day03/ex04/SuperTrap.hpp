#ifndef CPP_BOOTCAMP_SUPERTRAP_H
#define CPP_BOOTCAMP_SUPERTRAP_H


#include "NinjaTrap.hpp"

class SuperTrap : public NinjaTrap, public FragTrap {

public:
	SuperTrap();
	SuperTrap(const SuperTrap &src);
	SuperTrap(const std::string name);
	~SuperTrap();

	void initSuper(std::string name);

private:
};


#endif //CPP_BOOTCAMP_SUPERTRAP_H
