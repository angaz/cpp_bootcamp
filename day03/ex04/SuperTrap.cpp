#include "SuperTrap.hpp"

#include "NinjaTrap.hpp"
#include "FragTrap.hpp"

SuperTrap::SuperTrap() {
	initSuper("The Nameless One");
};

SuperTrap::SuperTrap(std::string name):NinjaTrap(name), FragTrap(name) {
	initSuper(name);
};

SuperTrap::SuperTrap(const SuperTrap &src) {
	*this = src;
	std::srand(static_cast<unsigned int>(time(0) * clock()));
	std::cout << "(" + this->_type + ") " + this->_name + " is replicated\n";
};

SuperTrap::~SuperTrap() {
	std::cout << "(" + this->_type + ") " + this->_name + ": am I dying?\n";
};


void SuperTrap::initSuper(std::string name) {
	this->_hp = 100;
	this->_maxHP = 100;
	this->_energy = 120;
	this->_maxEnergy = 120;
	this->_name = name;
	this->_type = "S00PR-TP";
	this->_level = 1;
	this->_meleeDamage = 60;
	this->_rangedDamage = 20;
	this->_armour = 5;
	std::srand(static_cast<unsigned int>(time(0) * clock()));
	std::cout << "(" + this->_type + ") " + this->_name + ": " + "Ha ha ha! Fall before your robot overlord!\n";
}