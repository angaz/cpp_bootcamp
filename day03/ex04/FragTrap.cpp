#include "FragTrap.hpp"

const std::string FragTrap::_actions[N_ACTIONS] = {
		"Clap-In-The-Box",
		"Gun Wizard",
		"Torgue Fiesta",
		"Pirate Ship Mode",
		"One Shot Wonder"
};

const std::string FragTrap::_sayings[N_SAYINGS] = {
		"Are... are you my father?",
		"Are you god? Am I dead?",
		"O-KAY! Thanks for giving me a second chance, God. I really appreciate it.",
		"Hey everybody! Check out my package!",
		"Let's get this party started!",
		"Glitching weirdness is a term of endearment, right?",
		"Recompiling my combat code!",
		"This time it'll be awesome, I promise!",
		"Look out everybody! Things are about to get awesome!"
};

FragTrap::FragTrap(void) {
	initFrag("The nameless one");
}

FragTrap::FragTrap(const std::string name) {
	initFrag(name);
}

void FragTrap::initFrag(std::string name) {
	this->_hp = 100;
	this->_maxHP = 100;
	this->_energy = 100;
	this->_maxEnergy = 100;
	this->_level = 1;
	this->_name = name;
	this->_type = "FR4G-TP";
	this->_meleeDamage = 30;
	this->_rangedDamage = 20;
	this->_armour = 5;
	std::srand(static_cast<unsigned int>(time(0) * clock()));
	std::cout << "(" + this->_type + ") " + this->_name + " is constructed\n";
	std::cout << "(" + this->_type + ") " + this->_name + " \"" + this->_sayings[std::rand() % N_SAYINGS] + "\"\n";
}

FragTrap::FragTrap(FragTrap const &src) {
	*this = src;
	std::srand(static_cast<unsigned int>(time(0) * clock()));
	std::cout << "(" + this->_type + ") " + this->_name + " is replicated\n";
	std::cout << "(" + this->_type + ") " + this->_name + " \"" + this->_sayings[std::rand() % N_SAYINGS] + "\"\n";
}

FragTrap::~FragTrap() {
	std::cout << "(" + this->_type + ") " + this->_name + " the FragTrap has been deconstructed for parts\n";
}

void	FragTrap::vaulthunter_dot_exe(std::string const & target) {
	std::cout << "(" + this->_type + ") " + this->_name + " special attacks ( " + this->_actions[std::rand() % N_ACTIONS] + " ) " + target + '\n';

	this->_energy -= max(this->_energy - 25, 0);
}

