#ifndef CPP_BOOTCAMP_FRAGTRAP_HPP
#define CPP_BOOTCAMP_FRAGTRAP_HPP

#include <string>
#include <iostream>
#include <cstdlib>

const unsigned int nActions = 5;
const unsigned int nSayings = 9;

class FragTrap {
public:
	FragTrap(void);
	FragTrap(const std::string name);
	FragTrap(FragTrap const &src);
	~FragTrap(void);

	FragTrap &operator=(const FragTrap &rhs);

	void	rangedAttack(const std::string &target) const;
	void	meleeAttack(const std::string &target) const;
	void	takeDamage(unsigned int amount);
	void	beRepaired(unsigned int amount);
	void	vaulthunter_dot_exe(const std::string &target);

	void	setName(const std::string newName);

private:
	unsigned int	_hp;
	unsigned int	_maxHP;
	unsigned int	_energy;
	unsigned int	_maxEnergy;
	unsigned int	_level;
	std::string		_name;
	unsigned int	_meleeDamage;
	unsigned int	_rangedDamage;
	unsigned int	_armour;

	static const std::string _actions[];
	static const std::string _sayings[];
};

#endif //CPP_BOOTCAMP_FRAGTRAP_HPP
