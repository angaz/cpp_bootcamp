#include "ScavTrap.hpp"

int main() {
	ScavTrap scavTrap = ScavTrap::ScavTrap("trapMan");

	scavTrap.rangedAttack("a Bullymong");
	scavTrap.meleeAttack("a skag");
	scavTrap.takeDamage(50);
	scavTrap.beRepaired(60);
	scavTrap.challengeNewcomer();

	return (0);
}