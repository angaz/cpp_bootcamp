//
// Created by Angus BURROUGHS on 2017/05/29.
//

#ifndef CPP_BOOTCAMP_IMININGLASER_H
#define CPP_BOOTCAMP_IMININGLASER_H

class IMiningLaser
{
public:
	virtual ~IMiningLaser() {}
	virtual void mine(IAsteroid* asteroid) = 0;
};

#endif //CPP_BOOTCAMP_IMININGLASER_H
