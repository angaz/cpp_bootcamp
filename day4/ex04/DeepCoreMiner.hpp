//
// Created by Angus BURROUGHS on 2017/05/29.
//

#ifndef CPP_BOOTCAMP_DEEPCOREMINER_H
#define CPP_BOOTCAMP_DEEPCOREMINER_H

#include "IMiningLaser.hpp"
#include "IAsteroid.hpp"

class DeepCoreMiner: public IMiningLaser {

public:
	DeepCoreMiner();
	DeepCoreMiner(const DeepCoreMiner &src);
	~DeepCoreMiner();
	&operator=(const DeepCoreMiner &rhs);

	void mine(IAsteroid *asteroid);
};


#endif //CPP_BOOTCAMP_DEEPCOREMINER_H
