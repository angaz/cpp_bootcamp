//
// Created by Angus BURROUGHS on 2017/05/29.
//
#ifndef I_ASTEROID_HPP
# define I_ASTEROID_HPP


class IAsteroid
{
public:
	virtual ~IAsteroid() {}
	virtual std::string beMined(IMiningLaser *) const = 0;
	virtual std::string getName() const = 0;
};

#endif

