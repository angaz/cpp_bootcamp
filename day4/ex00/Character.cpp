/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmaske <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/27 04:10:07 by tmaske            #+#    #+#             */
/*   Updated: 2017/05/27 04:10:09 by tmaske           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Character.hpp"

Character::Character() : name(randomName()) {
	std::cout << "<ANNOUNCER>A new Character has joined the game by means of summoning.\n";
}

Character::Character(std::string name) : name(name) {
	std::cout << "<ANNOUNCER>A new Character has joined the game by means of spawning.\n";
}

Character::Character(Character const & src) {
	*this = src;
	std::cout << "<ANNOUNCER>A new Character has joined the game by means of cloning.\n";
}

Character::~Character() {
	std::cout << "<ANNOUNCER>A Character has left the game.\n";
}
Character &		Character::operator=(Character const & src) {
	this->name = src.getName();
	return (*this);
}

std::string Character::getName() const {
	return (this->name);
}

void		Character::setName(std::string name) {
	this->name = name;
}

std::string     Character::randomName() const {
    std::string names[] = {"Fragger", "Bukko", "Fancy Jack", "Chimmy Changa", "2Lit2Quit", "SL4P-TP", "F4P-TP", "Mega Interplanetary Ninja Assassin", "CU5TM-TP", "L33T-TP"};
    return (names[(rand() % 10)]);
}
