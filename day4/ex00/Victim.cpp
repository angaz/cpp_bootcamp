/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Victim.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmaske <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/27 04:07:52 by tmaske            #+#    #+#             */
/*   Updated: 2017/05/27 04:07:55 by tmaske           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Victim.hpp"

Victim::Victim() : Team() {
	std::cout << "Some random victim called " << this->name << " just popped !\n";
}

Victim::Victim(std::string name) : Team(name) {
	std::cout << "Some random victim called " << this->name << " just popped !\n";
}

Victim::Victim(std::string name, std::string team) : Team(name, team) {
	std::cout << "Some random victim called " << this->name << " just popped !\n";
}

Victim::Victim(Victim const & src) {
	*this = src;
	std::cout << "Some random victim called " << this->name << " just popped !\n";
}

Victim::~Victim() {
	std::cout << "Victim " << this->name << " just died for no apparent reason !\n";
}

Victim &	Victim::operator=(Victim const & src) {
	this->name = src.getName();
	this->team = src.getTeam();
	this->enemyTeam = src.getEnemyTeam();
	return (*this);
}

void		Victim::getPolymorphed() const {
	std::cout << this->name << " has been turned into a cute little sheep !\n";
}

std::ostream & operator<<( std::ostream & o, Victim const & rhs) {
	o << "I'm " << rhs.getName() << " and I like otters !\n";
	return o;
}