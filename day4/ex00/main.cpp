/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmaske <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/25 22:26:53 by tmaske            #+#    #+#             */
/*   Updated: 2017/05/25 22:26:54 by tmaske           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Sorcerer.hpp"
#include "Peon.hpp"

int main( void ) {
    srand(time(NULL));
    Sorcerer sor1;
    Sorcerer *sor2 = new Sorcerer("Gaybeard", "The Man Lover");
    Sorcerer sor3 = Sorcerer("Malak", "The Hater", "Baddie");
    std::cout << "\n<ANNOUNCER>Sorcerer's! Identify!\n";
    std::cout << sor1 << "I hate the " << sor1.getEnemyTeam() << "'s so much I just can't wait to morph them !\n";
    std::cout << *sor2 << "I hate the " << sor2->getEnemyTeam() << "'s so much I just can't wait to morph them !\n";
    std::cout << sor3 << "I hate the " << sor3.getEnemyTeam() << "'s so much I just can't wait to morph them !\n";
    std::cout << "\n<ANNOUNCER>I have an 'assignment' for one of you unlucky ones...\n";
    sor1 = sor3;
    std::cout << sor1 << "I hate the " << sor1.getEnemyTeam() << "'s so much I just can't wait to morph them !\n";
    std::cout << sor3 << "I hate the " << sor3.getEnemyTeam() << "'s so much I just can't wait to morph them !\n";
    std::cout << "\n<ANNOUNCER>Let's get the others in shall we?\n";
    Victim vic1;
    Victim *vic2 = new Victim("Arno");
    Victim vic3 = Victim("Jason", "Baddie");
    std::cout << "\n<ANNOUNCER>Ahhhhh what do we have here?\n";
    std::cout << vic1 << "I hate the " << vic1.getEnemyTeam() << "'s so much I just can't wait to morph them !\n";
    std::cout << *vic2 << "I hate the " << vic2->getEnemyTeam() << "'s so much I just can't wait to morph them !\n";
    std::cout << vic3 << "I hate the " << vic3.getEnemyTeam() << "'s so much I just can't wait to morph them !\n";
    std::cout << "\n<ANNOUNCER>I have an assignment for one of you unlucky ones...\n";
    vic1 = vic3;
    std::cout << vic1 << "I hate the " << vic1.getEnemyTeam() << "'s so much I just can't wait to morph them !\n";
    std::cout << vic3 << "I hate the " << vic3.getEnemyTeam() << "'s so much I just can't wait to morph them !\n";
    std::cout << "\n<ANNOUNCER>And finally, the Peons.\n";
    Peon p1;
    Peon *p2 = new Peon("Jahne");
    Peon p3 = Peon("Lourens", "Baddie");
    std::cout << "\n<ANNOUNCER>Ohhhhhh so cute?\n";
    std::cout << p1 << "I hate the " << p1.getEnemyTeam() << "'s so much I just can't wait to morph them !\n";
    std::cout << *p2 << "I hate the " << p2->getEnemyTeam() << "'s so much I just can't wait to morph them !\n";
    std::cout << p3 << "I hate the " << p3.getEnemyTeam() << "'s so much I just can't wait to morph them !\n";
    std::cout << "\n<ANNOUNCER>I have an assignment for one of you unlucky ones...\n";
    p1 = p3;
    std::cout << p1 << "I hate the " << p1.getEnemyTeam() << "'s so much I just can't wait to morph them !\n";
    std::cout << p3 << "I hate the " << p3.getEnemyTeam() << "'s so much I just can't wait to morph them !\n";
    std::cout << "\n<ANNOUNCER>It's morphong time!\n";
    sor1.polymorph(vic1);
    sor2.polymorph(vic3);
    sor1.polymorph(p1);
    sor2.polymorph(p3);
    std::cout << "\n<ANNOUNCER>ENOUGH!\n";
    delete sor2;
    delete vic2;
    delete p2;
    return 0;
}