/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Team.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmaske <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/28 13:45:41 by tmaske            #+#    #+#             */
/*   Updated: 2017/05/28 13:45:42 by tmaske           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TEAM_HPP
#define TEAM_HPP

#include "Character.hpp"

class Team : public Character
{
public:
	Team();
	Team(std::string name);
    Team(std::string name, std::string team);
    Team(Team const & src);
    ~Team();
    Team & 			operator=(Team const & src);    
    std::string     randomTeam() const;
    std::string		getTeam() const;
    std::string		getEnemyTeam() const;
    void		    oppositeTeam();
    void			setTeam(std::string team);
    void			setEnemyTeam(std::string team);

protected:
    std::string		team;
    std::string		enemyTeam;
};

#endif