/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Sorcerer.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmaske <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/27 04:07:32 by tmaske            #+#    #+#             */
/*   Updated: 2017/05/27 04:07:34 by tmaske           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SORCERER_HPP
#define SORCERER_HPP

#include "Team.hpp"
#include "Character.hpp"
#include "Victim.hpp"

class Sorcerer : public Team
{
public:
	Sorcerer();
	Sorcerer(std::string name, std::string title);
    Sorcerer(std::string name, std::string title, std::string team);
    Sorcerer(Sorcerer const & src);
    ~Sorcerer();
    Sorcerer & 		operator=(Sorcerer const & src);
    std::string		getTitle() const;
    std::string     randomTitle() const;
    void			setTitle(std::string title);
    void            polymorph(Victim const & vic) const;

private:
    std::string     title;
};

std::ostream &  operator<<( std::ostream & o, Sorcerer const & rhs);

#endif