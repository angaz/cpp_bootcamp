/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Sorcerer.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmaske <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/27 04:07:38 by tmaske            #+#    #+#             */
/*   Updated: 2017/05/27 04:07:39 by tmaske           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Sorcerer.hpp"

Sorcerer::Sorcerer() : Team(), title(randomTitle()) {
	std::cout << this->name << ", " << this->title << ", is born !\n\n";
}

Sorcerer::Sorcerer(std::string name, std::string title) : Team(name), title(title) {
	std::cout << this->name << ", " << this->title << ", is born !\n\n";
}

Sorcerer::Sorcerer(std::string name, std::string title, std::string team) : Team(name, team), title(title) {
	std::cout << this->name << ", " << this->title << ", is born !\n\n";
}

Sorcerer::Sorcerer(Sorcerer const & src) {
	*this = src;
	std::cout << this->name << ", " << this->title << ", is born !\n\n";
}

Sorcerer::~Sorcerer() {
	std::cout << this->name << ", " << this->title << ", is dead. Consequences will never be the same !\n";
}

Sorcerer &	Sorcerer::operator=(Sorcerer const & src) {
	this->name = src.getName();
	this->team = src.getTeam();
	this->enemyTeam = src.getEnemyTeam();
	this->title = src.getTitle();
	return (*this);
}

std::string Sorcerer::getTitle() const {
	return (this->title);
}

void		Sorcerer::setTitle(std::string title) {
	this->title = title;
}

std::string     Sorcerer::randomTitle() const {
    std::string titles[] = {"The Beloved", "The Horny", "The Cuck", "The Hungry", "The Swaggest", "The Stormtrooper", "The Lazy", "That one guy too hipster for a title"};
    return (titles[(rand() % 8)]);
}

void Sorcerer::polymorph(Victim const & vic) const {
	vic.getPolymorphed();
}

std::ostream & operator<<( std::ostream & o, Sorcerer const & rhs) {
	o << "I am " << rhs.getName() << ", " << rhs.getTitle() << ", and I like ponies !\n";
	return o;
}