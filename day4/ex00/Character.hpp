/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmaske <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/27 04:10:12 by tmaske            #+#    #+#             */
/*   Updated: 2017/05/27 04:10:17 by tmaske           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHARACTER_HPP
#define CHARACTER_HPP

#include <iostream>

class Character
{
public:
	Character();
    Character(std::string name);
    Character(Character const & src);
    virtual ~Character() =0;
    Character & 			operator=(Character const & src);
    std::string				getName() const;
    std::string     		randomName() const;
    void					setName(std::string name);
    virtual std::string		getTeam() const =0;
    virtual std::string		getEnemyTeam() const =0;
    virtual void			setTeam(std::string) =0;
    virtual void			setEnemyTeam(std::string team) =0;

protected:
    std::string		name;
};

#endif