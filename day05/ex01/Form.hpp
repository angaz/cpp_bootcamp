#ifndef CPP_BOOTCAMP_FORM_H
#define CPP_BOOTCAMP_FORM_H

#include "Bureaucrat.hpp"

class Form {

public:
	Form();
	Form(std::string name, int signGrade, int execGrade);
	Form(const Form &src);
	~Form();
	Form &operator=(const Form &rhs);

	class GradeTooHighException : public std::exception {
	public:
		virtual const char* what() const throw();
	};

	class GradeTooLowException : public std::exception{
	public:
		virtual const char* what() const throw();
	};

	std::string const getName();
	int const getSignGrade();
	int const getExecGrade();
	bool const getSigned();

	void beSigned(Bureaucrat &bureaucrat);

private:
	std::string const 	_name;
	int const			_signGrade;
	int const			_execGrade;
	bool 				_signed;

};


#endif //CPP_BOOTCAMP_FORM_H
