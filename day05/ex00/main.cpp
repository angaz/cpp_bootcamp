#include "Bureaucrat.hpp"

int	main() {
	Bureaucrat boerie = Bureaucrat("boerie", 1);
	Bureaucrat betty = Bureaucrat("betty", 150);

	try {
		Bureaucrat bun = Bureaucrat("bun", 420);
		std::cout << bun << '\n';
	}
	catch(std::exception &e) {
		std::cout << e.what() << '\n';
	}

	try {
		Bureaucrat bear = Bureaucrat("bear", -69);
		std::cout << bear << '\n';
	}
	catch(std::exception &e) {
		std::cout << e.what() << '\n';
	}

	try {
		boerie.raiseGrade();
		std::cout << boerie << '\n';
	}
	catch(std::exception &e) {
		std::cout << e.what() << '\n';
	}


	try {
		betty.lowerGrade();
		std::cout << betty << '\n';
	}
	catch(std::exception &e) {
		std::cout << e.what() << '\n';
	}

	return 0;
}