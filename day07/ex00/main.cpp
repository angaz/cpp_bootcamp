#include <iostream>
#include <string>

#include "aFewFunctions.hpp"


int main(){
	int a = 69;
	int b = 420;

	std::string str1 = "first";
	std::string str2 = "second";


	std::cout << "Initial Values" << '\n';
	std::cout << "A: " << a << " B: " << b << '\n';

	::swap(a, b);
	std::cout << "Swapped Values" << '\n';
	std::cout << "A: " << a << " B: " << b << '\n';

	std::cout << "Min value: " << ::min(a, b) << '\n';
	std::cout << "Max value: " << ::max(a, b) << '\n';

	std::cout << "\nInitial Strings" << '\n';
	std::cout << "1: " << str1 << " 2: " << str2 << '\n';

	::swap(str1, str2);
	std::cout << "Swapped Strings" << '\n';
	std::cout << "1: " << str1 << " 2: " << str2 << '\n';

	return 0;
}