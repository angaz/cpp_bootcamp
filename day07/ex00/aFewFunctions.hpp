#ifndef CPP_BOOTCAMP_AFEWFUNCTIONS_H
#define CPP_BOOTCAMP_AFEWFUNCTIONS_H

template <class T>
void swap(T& a, T& b){
	T tmp = a;
	a = b;
	b = tmp;
}

template <class T>
T min(T& a, T& b){
	return (a < b) ? a : b;
}

template <class T>
T max(T& a, T& b){
	return (a > b) ? a : b;
}

#endif //CPP_BOOTCAMP_AFEWFUNCTIONS_H
