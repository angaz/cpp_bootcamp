#include <string>
#include <iostream>
#include <ctype.h>

template <class T>
void iter(T arr[], int len, void f(T &t)) {
	for (int i = 0; i < len; i++) {
		f(arr[i]);
	}
}

template <class T>
void ab_increment(T &i){
	i++;
}

template<class T>
void ab_print(T a) {
	std::cout << a;
}

int main() {
	char arrChar[] = {'a', 'b', 'c'};
	int	arrInt[] = {1, 2, 3};

	std::cout << "Chars before iter + increment\n";
	iter(arrChar, 3, ab_print);

	iter(arrChar, 3, ab_increment);

	std::cout << "\nChars after iter + increment\n";
	iter(arrChar, 3, ab_print);

	std::cout << "\nInts before iter + increment\n";
	iter(arrInt, 3, ab_print);

	iter(arrInt, 3, ab_increment);

	std::cout << "\nChars after iter + increment\n";
	iter(arrInt, 3, ab_print);
	std::cout << '\n';

	return 0;
}